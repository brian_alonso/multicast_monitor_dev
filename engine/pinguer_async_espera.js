var _ = require('underscore');
var mongoose   = require('mongoose');
mongoose.connect('mongodb://db:27017/app');
var ping = require ("net-ping");
var Historico = require('./models/historico');
var Client = require('./models/client');
var io = require('socket.io-client');
var async = require('async');



var socket = io.connect('http://backend:8081/', (err,status)=> console.log(status));
//   socket.on('news', function (data) {
//     console.log(data);
//     socket.emit('my other event', { my: 'data' });
//   });




socket.on('connect_failed', function(){
    console.log("*****************");
    console.log('Connection Failed');
});

socket.on('connect', function(){
    console.log("*****************");
    console.log('Connected');
});

socket.on('connect_timeout', function(){
    console.log("*****************");
    console.log('Connected');
});

var session = ping.createSession({retries: 4});





let clients = []

        Client.find(function(err,c){
            if(err)
                console.log("error");
            else {
                let clients = c
                
               let clientsEspera = clients.filter(function(el, i, array){
                    if(el.estado==="Espera")
                        return true;
                    else
                        return false;
                })
                
                
                async.eachLimit(clientsEspera, 12, function(client, finish){ 
                    
                    
                    
                    session.pingHost (client.ip , function (error, target) {
                        let estado = ""
                        if (error){
                            if (error instanceof ping.RequestTimedOutError){
                                estado = "Down"
                             //   console.log (target + ": Not alive");
                            }
                            else {
                                estado = "Down"
                          //     console.log (target + ": " + error.toString ());
                            }
                        }
                        else {
                            estado = "Up"
                         //   console.log (target + ": Alive");
                        }
                        
                        
                    
                        if(estado==="Down" && client.situacion !=="Desactivado" ){
                            console.log("Desactivando cliente en espera", client.cliente);
                            desactivarClient(client.id, finish);
                                
                        } else if(client.situacion!=="Down" && estado==="Up") {
                            activarClient(client._id, finish);
                        }    
                        else {
                            finish();    
                        }
                        
                        
                    }) ; //session ping
                    
                }
                    , function(err){
                        console.log("Fin de iteración");
                       
                        if(err){
                            console.log(err)
                        }
                        else{
                            socket.emit('heart_beat', {"status": "ok"} , function(ok){
                              console.log("Socket emit ok");
                              process.exit();
                            });

                        }
                    }
 
                    
                 );
} })



function activarClient(id, finish){
      Client.findById( id, function(err,client){
          if(err){
              console.log("Cliente no encontrado");
              finish();
          }
                
                client = _.extend(client,{estado: "Activo", ultimo_cambio: Date.now(), situacion: "Up"});
                var historico = new Historico();
                historico = _.extend(historico,{ situacion: client.situacion, ultimo_cambio: client.ultimo_cambio });
                historico.client = mongoose.Types.ObjectId(client._id);
                historico.save(function(err, historico){
                  if(err){
                    console.log(err);
                  }
                  else{
                    console.log("Save historico");
                    console.log(historico);
                  }
            
                });
      
            
            client.save(function(err, client){
                if(err){
                    console.log(err);
                    finish();
                }
                else{
                    console.log("Nueva situacion ", client.situacion, " at ", client.ultimo_cambio)
                    socket.emit('update_situacion', client );
                    console.log("Emit ok");
                    finish();
                }
            
                
            });
        });
    
}

function desactivarClient(id, finish){
      Client.findById( id, function(err,client){
          if(err){
              console.log("Cliente no encontrado");
              finish();
          }
                
                client = _.extend(client,{ ultimo_cambio: Date.now(), situacion: "Desactivado"});
                var historico = new Historico();
                historico = _.extend(historico,{ situacion: client.situacion, ultimo_cambio: client.ultimo_cambio });
                historico.client = mongoose.Types.ObjectId(client._id);
                historico.save(function(err, historico){
                  if(err){
                    console.log(err);
                  }
                  else{
                    console.log("Save historico");
                    console.log(historico);
                  }
            
                });
    

            client.save(function(err, client){
                if(err){
                    console.log(err);
                    finish();
                }
                else{
                    console.log("Nueva situacion ", client.situacion, " at ", client.ultimo_cambio)
                    socket.emit('update_situacion', client );
                    console.log("Emit ok");
                    finish();
                }
            
                
            });
        });
    
}
