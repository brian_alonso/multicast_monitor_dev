var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
    serv: {
        type: Number,
        required: true
    },
    cliente: {
        type: String,
        required: true
    },
    equipo: {
        type: String,
        required: true
    },
    ip : {
        type: String,
        required: true
    },
    interfaz: {
      type: String,
      required: true
    },
    estado: {
        type: String,
        default: "Desactivado"
    },
    alta: {
        type: Date,
        default: Date.now
    },
    ultimo_cambio: {
        type: Date,
        default: Date.now
    },
    situacion: {
        type: String,
        default: "Desactivado"
    },
    umbral: {
        type: Number,
        required: true
    },
    trafico: {
        type: Number,
        default: 0
    },
    tipo: {
        type: String,
        require: true
    },
    id_interface: {
        type: Number,
        required: true
    }

});

module.exports = mongoose.model('Client', ClientSchema);
