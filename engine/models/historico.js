var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Client = require('./client.js');
var HistoricoSchema = new Schema({
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Client',  
      required: true
    },
    situacion: {
        type: String,
        default: "Desactivado"
    },
    ultimo_cambio: {
        type: Date,
        default: Date.now
    },
    trafico:{
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Historico', HistoricoSchema);
