const snmpConnectorFactory = (deps) =>{
  //inject dependencies
  const snmpLibrary = deps.snmp
  const Promise = deps.bluebird
  const estado = ["", "Up","Down"]


  const snmp  = Promise.promisifyAll(snmpLibrary,{})

//  console.log(snmp);

  let getOid = (ip, oid) => {
      let session = new snmp.Session({
        host: ip,
        port: 161,
        community: "Arsat.refefo",
        timeouts:[1000,2000]
      })
      return session.getAsync({ oid: oid}).then((varbinds)=>varbinds[0].value)
  }


  

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


 let getCalculatedTraffic =   Promise.coroutine(function *(ip, id_interface, direction){
        let inout = {
          "Fuente": 10,//in
          "Cliente": 16,//out
          "in": 10,
          "out": 16
        }

        let state = yield getOid(ip, '.1.3.6.1.2.1.2.2.1.8.'+ id_interface)
        if(state!==1) throw new Error('Down')
        let speed = yield getOid(ip,'.1.3.6.1.2.1.2.2.1.5.'+ id_interface)
        if(speed < 1) throw new Error('Interface without Speed')
        let traffic_1 = yield getOid(ip, '.1.3.6.1.2.1.2.2.1.'+inout[direction]+'.'+id_interface)
        let time_1 = new Date();
        yield sleep(1000)
        let traffic_2 = yield getOid(ip, '.1.3.6.1.2.1.2.2.1.'+inout[direction]+'.'+id_interface)
        let time_2 = new Date();
        let delta_time =  (time_2 - time_1 ) / 1000
        let delta_traffic
        if(traffic_2 >= traffic_1){
         delta_traffic = traffic_2 - traffic_1 
        }else{
          delta_traffic = (Math.pow(2, 32) - traffic_1) + traffic_2
        }
        let percentage = Math.round((delta_traffic * 8 * 100 )/((delta_time) * speed))
        if(percentage < 0 || percentage > 100){
        console.log("__________________________________________________________")
        console.log({ip,time_1, time_2, delta_time, traffic_1, traffic_2, delta_traffic, speed })
        }
        return percentage
    })





  let getInterfacesHuawei = (ip)=>{
    let interfaces = {}
    let interfacesIndex = []
    let response = {}
    return new Promise((resolve, reject)=>{
      let session = new snmp.Session({
        host: ip,
        port: 161,
        community: "Arsat.refefo",
        timeouts:[1000,2000]
      })


      session.getAsync({ oid: [1,3,6,1,2,1,1,5,0]})
      .then((varbinds)=>{
        
        response.equipo = varbinds[0].value
        return session.getSubtreeAsync({ oid: [1, 3, 6, 1, 2, 1, 2,2,1,2]})
      })
      .then((varbinds)=>{
        // descriptions .1.3.6.1.2.1.2.2.1.2
         //.1.3.6.1.2.1.2.2
        //.1.3.6.1.4.1.2011.5.25.41.1.7.1.1.10

            console.log(varbinds[0].oid + ' = ' + varbinds[0].value + ' (' + varbinds[0].type + ')');
          console.log(varbinds);
            varbinds.forEach(function (vb) {
                // console.log(vb.oid + ' = ' + vb.value + ' (' + vb.type + ')');
                // console.log(vb.oid[vb.oid.length-1]);
                // console.log(vb)
                // console.log("---------------");
                interfaces[vb.oid[vb.oid.length-1]] = {name: vb.value}
                interfacesIndex.push(vb.oid[vb.oid.length-1])
            });
              return session.getSubtreeAsync({ oid: [1, 3, 6, 1, 2, 1, 2, 2, 1, 8] })
              //.1.3.6.1.2.1.2.2.1.8
      })
      .then((varbinds)=>{

        varbinds.forEach(function (vb) {

            //console.log(vb.oid + ' = ' + vb.value + ' (' + vb.type + ')');
            interfaces[vb.oid[vb.oid.length-1]]["state"] = estado[vb.value]
            interfaces[vb.oid[vb.oid.length-1]]["id_interface"] = vb.oid[vb.oid.length-1]
            //interfaces[vb.oid[vb.oid.length-1]]["oid_state"] = vb.oid.join(".")
        });
        return session.getSubtreeAsync({ oid: [1, 3, 6, 1, 4, 1, 2011,5,25,41,1,7,1,1,10] })
      })
      .then((varbinds)=>{
          varbinds.forEach(function (vb) {
              //console.log(vb.oid + ' = ' + vb.value + ' (' + vb.type + ')');
              interfaces[vb.oid[vb.oid.length-1]]["traffic_out"] = vb.value
          //    interfaces[vb.oid[vb.oid.length-1]]["oid_traffic_out"] = vb.oid.join(".")
          });
            return session.getSubtreeAsync({ oid: [1, 3, 6, 1, 4, 1, 2011,5,25,41,1,7,1,1,8] })
        })
      .then((varbinds)=>{
          varbinds.forEach(function (vb) {
              //console.log(vb.oid + ' = ' + vb.value + ' (' + vb.type + ')');
              interfaces[vb.oid[vb.oid.length-1]]["traffic_in"] = vb.value
            //  interfaces[vb.oid[vb.oid.length-1]]["oid_traffic_in"] = vb.oid.join(".")
          });
            let allInterfaces = interfacesIndex
              .map((el)=> interfaces[el])
              .filter((el)=> el.traffic_out || el.traffic_in )
              response.interfaces = allInterfaces
            resolve(response)
        })
        
        
        
      .catch((err)=>reject(err))
})}

  return {
    getInterfacesHuawei,
    getOid,
    getCalculatedTraffic,
  }
}

module.exports = snmpConnectorFactory
