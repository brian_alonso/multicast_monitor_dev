var _ = require('underscore');
var mongoose   = require('mongoose');
mongoose.connect('mongodb://db:27017/app');

var ping = require ("net-ping");
var Historico = require('./models/historico');
var Client = require('./models/client');
var io = require('socket.io-client');
var async = require('async');



var socket = io.connect('http://backend:8081/', (err,status)=> console.log(status));
//   socket.on('news', function (data) {
//     console.log(data);
//     socket.emit('my other event', { my: 'data' });
//   });




socket.on('connect_failed', function(){
    console.log('Connection Failed');
});

socket.on('connect', function(){
    console.log('Connected');
});

socket.on('connect_timeout', function(){
    console.log('Connected');
});

var session = ping.createSession({retries: 4});

let clients = []

        Client.find(function(err,c){
            if(err)
                console.log("error");
            else {
                let clients = c
                
                let clientsActivos = clients.filter(function(el, i, array){
                    if(el.estado==="Activo")
                        return true;
                    else
                        return false;
                })
                
                let clientsDesactivados = clients.filter(function(el, i, array){
                    if(el.estado==="Desactivado")
                        return true;
                    else
                        return false;
                })
                
                clientsDesactivados.forEach(function(client){
                    if (client.situacion !== "Desactivado"){
                    updateClient(client._id, "Desactivado", false)
                    }
                })
                
                
                async.eachLimit(clientsActivos, 5, function(client, finish){ 
                    console.log("Iterando a ",client.cliente);
                    
                    
                    session.pingHost (client.ip , function (error, target) {
                        let estado = ""
                        if (error){
                            if (error instanceof ping.RequestTimedOutError){
                                estado = "Down"
                             //   console.log (target + ": Not alive");
                            }
                            else {
                                estado = "Down"
                          //     console.log (target + ": " + error.toString ());
                            }
                        }
                        else {
                            estado = "Up"
                         //   console.log (target + ": Alive");
                        }
                    
                        if(client.situacion === estado){
                            console.log("No hubo cambios para ", client.cliente);
                            finish();    
                        } else {
                            console.log("Cambio para", client.cliente);
                            updateClient(client._id, estado, finish);
                            
                        }
                        
                    }) ; //session ping
                    
                }
                    , function(err){
                        console.log("Fin de iteración");
                       
                        if(err){
                            console.log(err)
                        }
                        else{
                            socket.emit('heart_beat', {"status": "ok"} , function(ok){
                              console.log("Socket emit ok");
                              process.exit();
                            });

                        }
                    }
                    
                 );
} })


function updateClient(id,situacion, finish){
      console.log("Updating client ....")
      Client.findById( id, function(err,client){
                if(err){
                    console.log("Error en el update buscando por id");
                    console.log(err);
                    finish && finish();
                }
                console.log(situacion);
                if(client.situacion === situacion) {
                 finish && finish();
                 return false;   
                }
                client = _.extend(client,{situacion: situacion, ultimo_cambio: Date.now()});
                var historico = new Historico();
                historico = _.extend(historico,{ situacion: situacion, ultimo_cambio: client.ultimo_cambio });
                historico.client = mongoose.Types.ObjectId(client._id);
                historico.save(function(err, historico){
                  if(err){
                    console.log(err);
                  }
                  else{
                    console.log("Save historico");
                    console.log(historico);
                  }
            
                });
               
        
        console.log("Saving changes in db ....")
            client.save(function(err, client){
                if(err){
                    console.log(err);
                    
                }
                else{
                    console.log("Nueva situacion ", client.situacion, " at ", client.ultimo_cambio)
                    socket.emit('update_situacion', client );
                    console.log("Emit ok");
                    
                }
                finish && finish()
                
            });
        });
    
}
