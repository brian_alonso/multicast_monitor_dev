var _ = require('underscore');
var mongoose   = require('mongoose');
mongoose.connect('mongodb://db:27017/app');

//var ping = require ("net-ping");
var Historico = require('./models/historico');
var Client = require('./models/client');
var io = require('socket.io-client');
var async = require('async');
const snmpConnector = require("./snmp/snmpConnector")
//const estado = ["", "Up","Down"]



var socket = io.connect('http://backend:8081/', (err,status)=> console.log(status));




socket.on('connect_failed', function(){
    console.log('Connection Failed');
});

socket.on('connect', function(){
    console.log('Connected');
});

socket.on('connect_timeout', function(){
    console.log('Connected');
});



let clients = []

        Client.find(function(err,clients){
            if(err)
                console.log("error");
            else {
            //    console.log(":):):):):):):):):):):):):):):):):):):)")
             //   console.log(clients)
                let clientsActivos = clients.filter(function(el, i, array){
                    if(el.estado==="Activo")
                        return true;
                    else
                        return false;
                })
                
                let clientsDesactivados = clients.filter(function(el, i, array){
                    if(el.estado==="Desactivado")
                        return true;
                    else
                        return false;
                })
                
                clientsDesactivados.forEach(function(client){
                    if (client.situacion !== "Desactivado"){
                    updateClient(client._id, "Desactivado", false)
                    }
                })
                
                
                async.eachLimit(clientsActivos, 5, function(client, finish){ 
              //      console.log(client)
            //        console.log("Iterando a ",client.cliente);
                    let newSituation
                    let newTraffic = 0
                    if(client.ip && client.id_interface){
                        
                        snmpConnector
                        .getCalculatedTraffic(client.ip, client.id_interface, client.tipo)
                        .then(el=>{
                            newTraffic = el
                            if(newTraffic < parseInt(client.umbral)){
              //                  console.log("OOR")
                                newSituation = "OOR"
                            }else{
                                newSituation= "Up"
                            }
                        })
                        .catch(err=>{
                            newSituation=err.message
                            //finish()
                        })
                        .finally((el)=>{
                            if(client.situacion !== newSituation || client.trafico !== newTraffic){
                                updateClient(client._id,newSituation,newTraffic, finish)
                            }else{
                                finish()
                            }
                //            console.log(newSituation + " " + newTraffic)
                        })
                    
                    }else{
                        finish()    
                    }
                    
                    
                    
                }
                    , function(err){
                        console.log("Fin de iteración");
                       mongoose.connection.close()
                        if(err){
                            console.log(err)
                        }
                        else{
                            socket.emit('heart_beat', {"status": "ok"} , function(ok){
                              console.log("Socket emit ok");
                              process.exit();
                            });

                        }
                    }
                    
                 );
} })


function updateClient(id,situacion,trafico, finish){
   //   console.log("Updating client ....")
      Client.findById( id, function(err,client){
          //      console.log("____________________________________")
                if(err){
            //        console.log("Error en el update buscando por id");
       //             console.log(err);
                    finish && finish();
                }
       //         console.log(situacion);
                if(client.situacion === situacion && client.trafico == trafico) {
                 finish && finish();
                 return false;   
                }
                
                if(client.situacion !== situacion){
                    client = _.extend(client,{situacion: situacion, trafico: trafico, ultimo_cambio: Date.now()});
                }else if(client.trafico != trafico){
                    client = _.extend(client,{trafico: trafico });
                }
                //client = _.extend(client,{situacion: situacion, trafico: trafico, ultimo_cambio: Date.now()});
                var historico = new Historico();
                 historico = _.extend(historico,{ 
                     situacion: situacion, 
                     ultimo_cambio: Date.now(), 
                     trafico: trafico 
                     
                 });
                 historico.client = mongoose.Types.ObjectId(client._id);
                 historico.save(function(err, historico){
                   if(err){
          //           console.log(err);
                     finish && finish();
                   }
                   else{
             //        console.log("Save historico");
                 //    console.log(historico);
                     
        //console.log("Saving changes in db ....")
            client.save(function(err, client){
                if(err){
              //      console.log(err);
                    finish && finish();
                }
                else{
          //          console.log("Nueva situacion ", client.situacion, " at ", client.ultimo_cambio)
                    socket.emit('update_situacion', client );
            //        console.log("Emit ok");
                    finish && finish()
                }
                
                
            });
                   }
            
                 });
                 
               
        
        });
    
}


//commentn