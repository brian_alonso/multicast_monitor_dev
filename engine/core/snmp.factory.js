
const engineFactory =(deps) =>
{
//inyectando dependencias
const   pty = deps.pty
const   Promise = deps.Promise

//Variables globales
var equipo = 0
var allServices = []
var allInterfacesData = []
var equiposCaidos = []

//Inicializa el terminal
// que es global dentro del closure
var term = pty.spawn('bash', [], {
  name: 'xterm-color',
  cols: 80,
  rows: 30,
  cwd: process.env.HOME,
  env: process.env
});

//clases


var EngineSsh = function(equipos, parser, resolve, folder, handleData){
  var currentState = new NotLogged(this)
  this.folder = folder
  this.buffer = ""
  this.parser = parser
  this.resolve = resolve
  this.equipos = equipos
  this.handleData = handleData
  this.change = function(state){
    currentState = state
    currentState.go("")
  }

  this.start = function(data){
    currentState.go(data)
  }
}

var NotLogged = function(engine){
  this.engine = engine
  console.log("Logging in proxy server")
  this.go = function(data){
    if(data.indexOf("Password")> -1){
        term.write("Changeme_123\n")
    }
    if(data.indexOf("U2000-PROD:") > -1){
        console.log("Login Succesfully")
        engine.change(new Logged(engine))
    }
  }
}


var Logged = function(engine){
  this.engine = engine
  this.go = function(data){
  const equipos = this.engine.equipos
    if(equipo < equipos.length){
      console.log("\nQuedan " + (equipos.length - equipo) + " equipos" )
      console.log("Retrieving information from " + equipos[equipo].ip )
      term.write("/opt/work/retrieveok.sh "+equipos[equipo].ip+"\n")
      engine.change(new Retrieving(engine))
      equipo = equipo + 1
    }else{
      engine.change(new Finished(engine))
    }

  }
}

var Retrieving = function(engine){
  this.engine = engine

  this.go = function(data){
    process.stdout.write(".")
    engine.buffer = engine.buffer.concat(data)
    //console.log(data)
    if(engine.buffer.indexOf("_MSG#_END_OF_SCRIPT_SUCCEDED_!")> -1){
        console.log("\n")
        engine.change(new Parsing(engine))
    }
    if(engine.buffer.indexOf("_ERROR#_") > -1){
      let posError = data.indexOf("_ERROR#_")
      if(posError > -1) {
        console.log("Error for " + this.engine.equipos[equipo -1].ip + " " + this.engine.equipos[equipo -1].nombreEquipo)
        if(data.indexOf("CONN") > -1){
          this.engine.equipos[equipo - 1].error = "Conectividad"
        }else{
          this.engine.equipos[equipo - 1].error = "Loguin"
        }
        equiposCaidos.push(this.engine.equipos[equipo - 1])
        console.log("\n")
        engine.change(new Logged(engine))
      }
    }
  }//end func



}

var Parsing = function(engine){
//  console.log(engine.buffer)
  this.engine = engine
  this.engine.handleData(this.engine.buffer,this.engine.equipos[equipo -1].ip)
  let services = this.engine.parser.parse(engine.buffer,this.engine.equipos[equipo -1].ip)
  let everything = this.engine.parser.parseAll(engine.buffer,this.engine.equipos[equipo -1].ip)
  allServices = allServices.concat(services)
  allInterfacesData = allInterfacesData.concat(everything)
  engine.buffer = " "
  this.go = function(data){
    if(services !== undefined){
        engine.change(new Logged(engine))
    }
  }
}

var Finished = function(engine){
  this.engine = engine
  let allData = [allServices, equiposCaidos,allInterfacesData]
  this.engine.resolve(allData)
  this.go = function(){
    console.log("Finish")
  }
}

return{
    start: (equipos, parser, handleData)=>{
        return myPromise = new Promise(function(resolve, reject){
            engine = new EngineSsh(equipos, parser, resolve,"",handleData)
            term.resize(100, 40);
            term.on('data', function(data) {
              engine.start(data)
            })
            //term.socket.write("ifconfig -a | grep 192\n")
            term.socket.write("ssh root@192.168.48.82\r")
            console.log(term.process);
        })


    }
}



}
module.exports = engineFactory



// estados
// NotLogged -> Logged ->Retrieving -> Parsing -> Logged
// when there are not any equipments
// logged -> finished
