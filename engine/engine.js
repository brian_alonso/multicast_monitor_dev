const exec = require('child_process').exec;

//every minute execute pinguer_async.js

var CronJob = require('cron').CronJob;
new CronJob('*/10 * * * * *', function() {
  console.log("Arrancando");
   const child = exec('node snmp_async.js',
     (error, stdout, stderr) => {
         console.log(`stdout: ${stdout}`);
         console.log(`stderr: ${stderr}`);
         if (error !== null) {
             console.log(`exec error: ${error}`);
         }
  });
}, null, true, 'America/Los_Angeles');


//every hour execute pinguer_async_espera.js

// new CronJob('*/60 * * * * *', function() {
//   console.log("Arrancando");
//   const child = exec('node pinguer_async_espera.js',
//     (error, stdout, stderr) => {
//         console.log(`stdout: ${stdout}`);
//         console.log(`stderr: ${stderr}`);
//         if (error !== null) {
//             console.log(`exec error: ${error}`);
//         }
// });
// }, null, true, 'America/Los_Angeles');