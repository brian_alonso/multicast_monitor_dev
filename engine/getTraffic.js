const snmpConnector = require("./snmp/snmpConnector")
const bluebird = require('bluebird')


let client = {
    ip: "181.209.14.133",
    id_interface: 1,
    tipo: "Fuente"
}
let inout = {
    "Fuente": 10,//in
    "Cliente": 16//out
}


function sleep(ms) {
  return new bluebird(resolve => setTimeout(resolve, ms));
}


 let getTraffic =   bluebird.coroutine(function *(client){
        let state = yield snmpConnector.getOid(client.ip, '.1.3.6.1.2.1.2.2.1.8.'+client.id_interface)
        if(state!==1) throw new Error('Down')
        let speed = yield snmpConnector.getOid(client.ip,'.1.3.6.1.2.1.2.2.1.5.'+client.id_interface)
        if(speed < 1) throw new Error('Interface without Speed')
        let traffic_1 = yield snmpConnector.getOid(client.ip, '.1.3.6.1.2.1.2.2.1.'+inout[client.tipo]+'.'+client.id_interface)
        let time_1 = new Date();
        yield sleep(1000)
        let traffic_2 = yield snmpConnector.getOid(client.ip, '.1.3.6.1.2.1.2.2.1.'+inout[client.tipo]+'.'+client.id_interface)
        let time_2 = new Date();
        let delta_time =  (time_2 - time_1 ) / 1000
        let delta_traffic = traffic_2 - traffic_1
        let percentage = (delta_traffic * 8 * 100 )/((delta_time) * speed)
        let realPercentage 
        if(client.tipo==="Fuente")
            realPercentage = yield  snmpConnector.getOid(client.ip, '.1.3.6.1.4.1.2011.5.25.41.1.7.1.1.8.'+client.id_interface)
        else
            realPercentage = yield snmpConnector.getOid(client.ip, '.1.3.6.1.4.1.2011.5.25.41.1.7.1.1.10.'+client.id_interface)    
        console.log({realPercentage, percentage})
        
        
        return percentage
    })
    
    
    
    getTraffic(client)
        .then((el)=>{
            console.log(el)
            process.exit()
        })
        .catch((err)=>{
        console.log(err)
        process.exit()
    })
