var express = require('express');
var Historico = require('../models/historico');
var _ = require('underscore');
var mongoose = require('mongoose');

var router = express.Router();

router.use(function(req, res, next){
  console.log("Algo esta pasando en la historico route");
  next();
});



router.route('/historicos')
    .post(function(req,res){
        var historico = new Historico();
        
 
        historico = _.extend(historico,req.body.historico);
        historico.client = mongoose.Types.ObjectId(historico.client);
        historico.save(function(err, historico){
            if(err){
                res.send(err);
            }
            else{
                res.json({ message: "Historico successfully added!", historico});    
            }
            
        });
    })
    .get(function(req,res){
        Historico.find(function(err,historicos){
            if(err)
                res.send(err);
            
            res.json(historicos);
        });
    });
    
router.route('/historicos/:historico_id')
    .get(function(req, res){
        Historico.findById(req.params.historico_id , function(err, historico){
            if(err)
                res.send(err);
            res.json(historico);
        });
    })
    
    .put(function(req,res) {
        
        Historico.findById(req.params.historico_id, function(err,historico){
                
                historico = _.extend(historico,req.body.historicoe);
            
             //historico = req.body.historico
            // historico.serv = req.body.serv;
            // historico.historicoe = req.body.historicoe;
            // historico.equipo = req.body.equipo;
            // historico.sitio = req.body.sitio;
            // historico.ip = req.body.ip;
            // historico.estado = req.body.estado;

            historico.save(function(err, historico){
                if(err){
                    res.send(err);
                }
                else{
                    res.json({ message: "Historico successfully modified!", historico});    
                }
            
                
            });
        });
        
    })
    .delete(function (req, res) {
        Historico.remove({
            _id: req.params.historico_id
        }, function (err, result) {
            if(err)
                res.send(err);
            res.json({message : "Successfully deleted", result});
        });
    });
    
 
module.exports = router;
