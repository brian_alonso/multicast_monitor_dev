var mongoose= require('mongoose');

var express = require('express');
var Client = require('../models/client');
var _ = require('underscore');
var Historico = require('../models/historico');

var router = express.Router();

router.use(function(req, res, next){
  console.log("Algo esta pasando en la api route");
  next();
});



router.route('/clients')
    .post(function(req,res){
        var client = new Client();
        
        
        // client.serv = req.body.serv;
        // client.cliente = req.body.cliente;
        // client.equipo = req.body.equipo;
        // client.sitio = req.body.sitio;
        // client.ip = req.body.ip;
        // client.estado = req.body.estado;
        //client = req.body.cliente
        client = _.extend(client,req.body.cliente);
        client.save(function(err, client){
            if(err){
                console.log(err)
                res.send(err);
            }
            else{
                res.json({ message: "Client successfully added!", client});    
            }
            
        });
    })
    .get(function(req,res){
        Client.find(function(err,clients){
            if(err)
                res.send(err);
            
            res.json(clients);
        });
    });
    
router.route('/clients/:client_id')
    .get(function(req, res){
        Client.findById(req.params.client_id , function(err, client){
            var newClient = null;
            if(err)
                res.send(err);
            if(client){
              Historico.find({ "client": mongoose.Types.ObjectId(client._id)})
              .select("ultimo_cambio _id situacion trafico")
              .sort({date: 'desc'})
              .lean()
              .exec( function(err,historicos){
                if(err)
                  res.send(err);
                newClient = client.toObject();
                newClient = _.extend(newClient, { "historicos": historicos});
                res.json(newClient);
              });
            }
         });
    })
    
    .put(function(req,res) {
        
        Client.findById(req.params.client_id, function(err,client){
                
                client = _.extend(client,req.body.cliente);
            
             //client = req.body.client
            // client.serv = req.body.serv;
            // client.cliente = req.body.cliente;
            // client.equipo = req.body.equipo;
            // client.sitio = req.body.sitio;
            // client.ip = req.body.ip;
            // client.estado = req.body.estado;

            client.save(function(err, client){
                if(err){
                    res.send(err);
                }
                else{
                    res.json({ message: "Client successfully modified!", client});    
                }
            
                
            });
        });
        
    })
    .delete(function (req, res) {
        Client.remove({
            _id: req.params.client_id
        }, function (err, result) {
            if(err)
                res.send(err);
            res.json({message : "Successfully deleted", result});
        });
    });
    
 
module.exports = router;
