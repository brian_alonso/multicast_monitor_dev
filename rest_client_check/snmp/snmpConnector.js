/**
 * Creates an atomic services which will be analized.
 * It tells you what to do in the network???
 * @module Engine
 */
//dependecy
let snmp = require('snmp-native');
var bluebird = require('bluebird')

const deps = {
    snmp,
    bluebird
}

const factory = require('./snmpConnector.factory')

module.exports = factory(deps)
