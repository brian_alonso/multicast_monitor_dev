var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');

console.log(process.env.NODE_ENV);

mongoose.connect('mongodb://db:27017/app');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


var port = 8081; //process.env.PORT 
if(process.env.NODE_ENV === 'test'){
  port=666
}
var client_router = require('./app/routes/client.js');
var historico_router = require('./app/routes/historico.js');
let get_interfaces_huawei_router = require('./app/routes/getInterfacesHuawei')

   

app.use('/api', client_router);
app.use('/api', historico_router);
app.use('/api', get_interfaces_huawei_router)


//socket.io config


// setting up socket.io
var server = require('http').Server(app);
var io = require('socket.io')(server);




io.on('connection', function (socket) {
  console.log("Connection to socket");
  socket.on('update_situacion', function(data){
      socket.broadcast.emit('update_situacion', data);
      console.log("emit");
  });
  socket.on('heart_beat', function(data,fn){
      socket.broadcast.emit('heart_beat', {"status":"ok"});
      console.log("emit heart_beat");
      fn('ok');
  });

});


//server = app.listen(port);

server.listen(port);


console.log("Listen on ...");

module.exports = server
