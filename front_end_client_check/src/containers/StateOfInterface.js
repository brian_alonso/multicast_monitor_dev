import React, { Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';


class StateOfInterface extends Component{


  
  render(){
   let situacion =  " "

    switch (this.props.interface_state.get("ok")){
      case "Up":
        situacion = <span className="icon has-text-success"><i className="fa fa-thumbs-up" aria-hidden="true"></i></span>
        break
      case "Down":
        situacion= <span className="icon has-text-danger"><i className="fa fa-thumbs-down" aria-hidden="true"></i></span>
        break
      case "Waiting":
        situacion= <span className="icon has-text-info"><i className="fa fa-cloud-download" aria-hidden="true"></i></span>
       break
      }     
     
     
     return( 
      <span style={ {color: "white"} }>Estado Interfaz: {situacion}</span>
   );
  }
}


function mapStateToProps(state){
        return {
            interface_state: state.interface_state_ui
        }
    }




//export default ClientList

export default connect(mapStateToProps )(StateOfInterface) ; //now it is a smart component, container


