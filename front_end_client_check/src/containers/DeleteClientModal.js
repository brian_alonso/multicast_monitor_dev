import React, { Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {showDeleteClientModal, hideDeleteClientModal, setDeleteClientModalLoading} from '../actions/delete_client_actions'
import {deleteClientRest } from '../actions/client_rest_actions'
import Modal from '../components/Modal'


class DeleteClientModal extends Component {
  constructor(props){
    super(props)
    this.onDelete = this.onDelete.bind(this);
  }

   onDelete() {
    
    console.log(this.props.delete_client_ui.get('delete_client_id')) 
    this.deleteClient()
    
  }

    
  deleteClient(){
      
            
      this.props.setDeleteClientModalLoading(true)
      this.props.deleteClientRest(this.props.delete_client_ui.get('delete_client_id')).then(
        (data) => {
                    this.props.setDeleteClientModalLoading(false)
                    this.props.hideDeleteClientModal()
                    
                    
        },
        (err) => { console.log(err) 
                        this.props.setDeleteClientModalLoading(false)
                    this.props.hideDeleteClientModal()
                    
                        }
                    
        
            
      )
      
  }
  
  
  render(){ 
    
    
    return (
    <Modal 
      showModal={this.props.delete_client_ui.get('showDeleteClientModalFlag')}
      title="Confirmacion"
      onClose={this.props.hideDeleteClientModal}
      onConfirm={this.onDelete}
      confirmButtonMessage="Eliminar"
      confirmButtonClass="is-danger"
      cancelButtonClass="is-dark"
      cancelButtonMessage="Cancelar"
      loading={this.props.delete_client_ui.get('delete_modal_loading')}
    >
    <h1>Está seguro que desea eliminar este registro?</h1>
    </Modal>
      )
     
     
      
  }
}

function mapStateToProps(state){
        return {
            delete_client_ui: state.delete_client_ui
        }
    }

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    showDeleteClientModal: showDeleteClientModal,
    hideDeleteClientModal: hideDeleteClientModal,
    deleteClientRest: deleteClientRest,
    setDeleteClientModalLoading: setDeleteClientModalLoading
  }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(DeleteClientModal);





