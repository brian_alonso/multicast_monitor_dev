import moment from 'moment'
 
import {Map} from 'immutable';
import React, { Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {updateClient} from '../../actions'
import {fetchClients, toggleClient} from '../../actions/client_rest_actions.js'
import uuid from '../../helpers/generateUUID'
import Row from './Row.js'
import HeaderTable from './HeaderTable.js'
let esLocale = require('moment/locale/es')
moment.locale('es', esLocale)

class Table extends Component{
  
  constructor(props){
    super(props)
    this.state = {
      sortBy: 'serv',
      ascending: true
    }
    
    this.setField = this.setField.bind(this)
  }
  
  
  componentDidMount(){
      this.props.fetchClients();
  }
  
  setField(field){
    if(field === this.state.sortBy){
      this.setState({ascending: !this.state.ascending})
    } 
    this.setState({sortBy: field.toLowerCase()})
  }
  
  render(){
    var clients = this.props.clients
    var filter = this.props.filter
    
    var filteredClients = clients.filter((cliente) =>{
      
      var passedfilter =  cliente.filter( (attr,key) => {
          let attrcopy = attr
          if (key === "checked") return false
          if (typeof attr === "number") attrcopy = attr + ''
          if (key === "alta") attrcopy = moment(attr).calendar()
          if (key === "historico") attrcopy = ""
          if (key !== "_id" && attrcopy && attrcopy.toLowerCase().includes(filter.toLowerCase())){
             return true
          }
	      return false
      });
      
      if (passedfilter.size > 0) return true
      
     return false
    });
  
  
    var sortedClients = sortTableBy(filteredClients, this.state.sortBy, this.state.ascending)  
    
    var listClients = sortedClients.map((client, index) => 
        <Row key={index} client={client} tableProps={this.props} handleChBoxClick={this.props.toggleClient} />  
    );
    
    return(
      <table className="table  is-narrow is-fullwidth">
             <HeaderTable setField={this.setField}/>
              <tbody>
                {listClients}
              </tbody>
      </table>
    );
  }
}




function toType(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}


function sortTableBy(info ,field, ascending){
  //ver de que tipo es 
  //ordenar según el tipo
  //aplicar asc o desc
  //returnar array
  
  //let sorted = info.sort((a,b)=> a.get('serv') - b.get('serv'))
  let sorted = info.sortBy((client) => client.get(field))
  return ascending ? sorted : sorted.reverse()
  
}




function mapStateToProps(state){
        return {
            clients: state.clients
        }
    }

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    fetchClients: fetchClients,
    updateClient: updateClient,
    toggleClient: toggleClient,
    
  }, dispatch)
}



//export default ClientList

export default connect(mapStateToProps, matchDispatchToProps)(Table) ; //now it is a smart component, container
