import React, { Component} from 'react'
import uuid from '../../helpers/generateUUID'






class HeaderTable extends Component{
  render(){
    
    var attrs = [
      { vista: "Servicio", logico: "serv"},
      { vista: "Cliente", logico: "cliente"},
      { vista: "Equipo" , logico: "equipo"}, 
      { vista: "Ip", logico: "ip"},
      { vista: "Interfaz", logico: "interfaz" }, 
      { vista: "Umbral", logico: "umbral" }, 
      { vista: "Estado", logico: "estado"},
      { vista: "Ultimo Cambio", logico: "ultimo_cambio"},
      { vista: "Diagnóstico", logico: "situacion"}
    ]
    
    var printTh = (attr) => <th key={uuid()} onClick={()=>{this.props.setField(attr.logico)}}>{attr.vista}</th>    
    
    
    var printFields = (attrs, printFunc) => {
       return  attrs.map( (attr)=> {
          return printFunc(attr);
        })
        
    }

    return(
      <thead>
        <tr>
          
          {printFields(attrs, printTh)}
          
          <th></th>
          <th></th>
          
        </tr>  
      </thead>
      
    );
  }
}

export default HeaderTable