import moment from 'moment'
import StateOfClient from './StateOfClient' 
import {Map} from 'immutable';
import React, { Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {updateClient} from '../actions'
import {fetchClients, toggleClient} from '../actions/client_rest_actions.js'
import uuid from '../helpers/generateUUID'
import EditClientButton from './EditClientButton'
import DeleteClientButton from './DeleteClientButton'

let esLocale = require('moment/locale/es')
moment.locale('es', esLocale)

function falseIfUndef(value){
  value === undefined ? false : value
}

class Table extends Component{
  
  constructor(props){
    super(props)
    this.state = {
      sortBy: 'serv',
      ascending: true
    }
    
    this.setField = this.setField.bind(this)
  }
  
  
  componentDidMount(){
      this.props.fetchClients();
  }
  
  setField(field){
    if(field === this.state.sortBy){
      this.setState({ascending: !this.state.ascending})
    } 
    this.setState({sortBy: field.toLowerCase()})
  }
  
  render(){
    var clients = this.props.clients
    var filter = this.props.filter
    
    var filteredClients = clients.filter((cliente) =>{
      
      var passedfilter =  cliente.filter( (attr,key) => {
          let attrcopy = attr
          if (key === "checked") return false
          if (typeof attr === "number") attrcopy = attr + ''
          if (key === "alta") attrcopy = moment(attr).calendar()
          if (key === "historico") attrcopy = ""
          if (key !== "_id" && attrcopy && attrcopy.toLowerCase().includes(filter.toLowerCase())){
             return true
          }
	      return false
      });
      
      if (passedfilter.size > 0) return true
      
     return false
    });
  
  
    var sortedClients = sortTableBy(filteredClients, this.state.sortBy, this.state.ascending)  
    
    var listClients = sortedClients.map((client, index) => 
        <Row key={index} client={client} tableProps={this.props} handleChBoxClick={this.props.toggleClient} />  
    );
    
    return(
      <table className="table  is-narrow is-fullwidth">
             <HeaderTable setField={this.setField}/>
              <tbody>
                {listClients}
              </tbody>
      </table>
    );
  }
}


class Row extends Component {
  constructor(props){
    super(props);
    this.state = { edit: false }
    this.handleClickEdit = this.handleClickEdit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.modifiedClient = Map(this.props.client)
  
  };
  
  
  
  
  handleClickEdit() {
    this.setState({edit: !this.state.edit})
    this.props.tableProps.updateClient(this.modifiedClient)
      //set_state
  }
  
  handleChange(attr, e){
      this.modifiedClient = this.modifiedClient.set(attr, e.target.value)
  }
  
  
  
  render() {


var attrs = ["cliente", "equipo" , "sitio" , "ip","email", "estado"]

var printTd = (immut,attr) => <td key={uuid()}>{immut.get(attr)}</td>    


var printFields = (immut,attrs, printFunc) => {
   return  attrs.map( (attr)=> {
      return printFunc(immut,attr);
    })
    
}


    
var   noEdit = () => (
              <tr>
              <td> <input 
                      onChange={(e)=>this.props.handleChBoxClick(client.get("_id")) } 
                      type="checkbox" 
                      checked={falseIfUndef(client.get("checked"))}/></td>
              <td>SERV-{client.get("serv")}</td>
              {printFields(client ,attrs, printTd)}
              
              <td style={{fontSize: "70%"}}>
                
                {moment(client.get("ultimo_cambio")).calendar()}
                
              </td>
		<td>
		<StateOfClient client={client} />
		</td>
              	<td>
                <EditClientButton client={client} />
                <DeleteClientButton client_id={client.get("_id")} />
              </td>
            </tr>
      )



var servStyle = {
      color: 'black',
      fontSize: "72%",
      paddingTop: "10%"
    };

   
    
    const client = this.props.client
    
    return noEdit();
    
  }
}

class HeaderTable extends Component{
  render(){
    
    var attrs = [
      { vista: "Servicio", logico: "serv"},
      { vista: "Cliente", logico: "cliente"},
      { vista: "Equipo" , logico: "equipo"}, 
      { vista: "Sitio" , logico: "sitio"},
      { vista: "Ip", logico: "ip"},
      { vista: "Email", logico: "email" }, 
      { vista: "Estado", logico: "estado"},
      { vista: "Ultimo Cambio", logico: "ultimo_cambio"},
      { vista: "Situación", logico: "situacion"}
    ]
    
    var printTh = (attr) => <th key={uuid()} onClick={()=>{this.props.setField(attr.logico)}}>{attr.vista}</th>    
    
    
    var printFields = (attrs, printFunc) => {
       return  attrs.map( (attr)=> {
          return printFunc(attr);
        })
        
    }

    return(
      <thead>
        <tr>
          <th></th>
          {printFields(attrs, printTh)}
          
          <th></th>
          <th></th>
          
        </tr>  
      </thead>
      
    );
  }
}
function toType(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}


function sortTableBy(info ,field, ascending){
  //ver de que tipo es 
  //ordenar según el tipo
  //aplicar asc o desc
  //returnar array
  
  //let sorted = info.sort((a,b)=> a.get('serv') - b.get('serv'))
  let sorted = info.sortBy((client) => client.get(field))
  return ascending ? sorted : sorted.reverse()
  
}




function mapStateToProps(state){
        return {
            clients: state.clients
        }
    }

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    fetchClients: fetchClients,
    updateClient: updateClient,
    toggleClient: toggleClient,
    
  }, dispatch)
}



//export default ClientList

export default connect(mapStateToProps, matchDispatchToProps)(Table) ; //now it is a smart component, container
