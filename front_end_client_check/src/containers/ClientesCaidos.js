import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {fetchClients} from '../actions/client_rest_actions.js'
import React, { Component } from 'react';




class ClientesCaidos extends Component {
  

  

  
  
  componentDidMount(){
      this.props.fetchClients();
  }
  
  filterSort(clients, situacion) {
    let filtered = clients.filter(
          (el) => el.get("situacion") === situacion
    )
    
    let sorted = filtered.sort(
        (b, a) => a.get('ultimo_cambio').localeCompare(b.get('ultimo_cambio'))    
    )
    return sorted
  }
  
  renderClient(clients){
      let situaciones = clients.map((cliente, index) => 
         <span key={cliente.get('_id')}>{cliente.get('serv')}, </span>
    );
    return situaciones
  }
      
  
  render(){
    
    let clients = this.props.clients
   
    
    let down_sorted = this.filterSort(clients,"Down"); 
    
    
    
    
    return (
        <div className="content">
        <br />
        <h3>Listado de clientes caídos para exportar a Máximo:</h3>
          <p> 
            {this.renderClient(down_sorted)}
          </p>
        </div>
      );
  }
}


function mapStateToProps(state){
        return {
            clients: state.clients
        }
    }

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    fetchClients: fetchClients

  }, dispatch)
}



//export default ClientList

export default connect(mapStateToProps, matchDispatchToProps)(ClientesCaidos) ; //now it is a smart component, container



