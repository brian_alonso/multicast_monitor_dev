import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { fetchClients } from '../actions/client_rest_actions.js'
import React, { Component } from 'react';
import State from '../components/State';
import Filtro from '../components/Filtro';
import moment from 'moment';




class StateView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filterString: ""
    }
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  handleFilterChange(value) {
    this.setState({ filterString: value })
  }



  componentWillMount() {
    this.props.fetchClients();
  }

  filterSort(clients, situacion) {
    let filtered = clients.filter(
      (el) => el.get("situacion") === situacion
    )

    let sorted = filtered.sort(
      (b, a) => a.get('ultimo_cambio').localeCompare(b.get('ultimo_cambio'))
    )
    return sorted
  }

  filterSortAbnormal(clients) {
    let filtered = clients.filter(
      (el) => el.get("situacion") !== "Up" && el.get("situacion") !== "Desactivado"
    )

    let sorted = filtered.sort(
      (b, a) => a.get('ultimo_cambio').localeCompare(b.get('ultimo_cambio'))
    )
    return sorted
  }



  renderClient(clients) {
    let situaciones = clients.map((cliente, index) =>

      <State 
            key={index} 
            estado={cliente.get('situacion')}
            nombreCliente={cliente.get('cliente')}
            serv = {cliente.get('serv')}
            ag = {cliente.get('equipo')}
            sitio = {cliente.get('sitio')}
            date = {cliente.get('ultimo_cambio')}
            ip = {cliente.get('ip')}
            _id = {cliente.get('_id')}
            umbral = { cliente.get('umbral')}
            interfaz= { cliente.get('interfaz')}
            trafico = { cliente.get('trafico')}
        />

    );
    return situaciones
  }


  render() {

    let clients = this.props.clients
    var filter = this.state.filterString;

    var filteredClients = clients.filter((cliente) => {
      if (cliente.get("tipo") !== "Cliente") return false
      var passedfilter = cliente.filter((attr, key) => {
        let attrcopy = attr
        if (typeof attr === "number") attrcopy = attr + ''
        if (key === "alta") attrcopy = moment(attr).calendar()
        if (key === "historico") attrcopy = ""
        if (key !== "_id" && attrcopy && attrcopy.toLowerCase().includes(filter.toLowerCase())) {
          return true
        }
        return false
      });

      if (passedfilter.size > 0) return true



      //   for(var key in cliente){
      //     console.log(key + " " + cliente)
      //     if (cliente.get(key) && cliente.get(key).toLowerCase().includes(filter.toLowerCase())){
      //       return true
      //     }
      //   }
      return false
    });

    var filteredFuentes = clients.filter((cliente) => {
      if (cliente.get("tipo") !== "Fuente") return false
      var passedfilter = cliente.filter((attr, key) => {
        let attrcopy = attr
        if (typeof attr === "number") attrcopy = attr + ''
        if (key === "alta") attrcopy = moment(attr).calendar()
        if (key === "historico") attrcopy = ""
        if (key !== "_id" && attrcopy && attrcopy.toLowerCase().includes(filter.toLowerCase())) {
          return true
        }
        return false
      });

      if (passedfilter.size > 0) return true



      //   for(var key in cliente){
      //     console.log(key + " " + cliente)
      //     if (cliente.get(key) && cliente.get(key).toLowerCase().includes(filter.toLowerCase())){
      //       return true
      //     }
      //   }
      return false
    });



    let abnormal_sorted = this.filterSortAbnormal(filteredClients);
    let up_sorted = this.filterSort(filteredClients, "Up");
    let desactivado_sorted = this.filterSort(filteredClients, "Desactivado");
    let fuentes_abnormal_sorted = this.filterSortAbnormal(filteredFuentes);
    let fuentes_up_sorted = this.filterSort(filteredFuentes, "Up");
    let fuentes_desactivado_sorted = this.filterSort(filteredFuentes, "Desactivado");



    return (
      <div>
        
            <br />
            <Filtro onFilterChange={this.handleFilterChange} filterString={this.state.filterString}/>
            <br />
            <div className="columns is-multiline">
              <div className="column is-narrow is-8-fullhd is-8-widescreen is-8-desktop is-8-tablet is-8-mobile">
                <h1 className="title">Fuentes</h1>
              </div>
            </div>
            <div className="columns is-multiline">
                {this.renderClient(filteredFuentes)}
            </div>
            <div className="columns is-multiline">
              <div className="column is-narrow is-3-fullhd is-3-widescreen is-4-desktop is-11-tablet is-11-mobile">
                <h1 className="title">Clientes</h1>
              </div>
            </div>
            <div className="columns is-multiline">
              
                {this.renderClient(abnormal_sorted)}
                {this.renderClient(up_sorted)}
                {this.renderClient(desactivado_sorted)}
              
            </div>
        </div>



    );
  }
}


function mapStateToProps(state) {
  return {
    clients: state.clients
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchClients: fetchClients

  }, dispatch)
}



//export default ClientList

export default connect(mapStateToProps, matchDispatchToProps)(StateView); //now it is a smart component, container
