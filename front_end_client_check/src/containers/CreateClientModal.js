import React, { Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {showEditModal, hideEditModal, setEditModalLoading} from '../actions/create_client_actions'
import {postNewClient, putClient, fetchInterfaces  } from '../actions/client_rest_actions'
import {clearInterfaceState  } from '../actions/interface_state_actions'
import Modal from '../components/Modal'
import NewClientForm from '../components/NewClientForm'
import EditClientForm from '../components/EditClientForm'

class CreateClientModal extends Component {
  constructor(props){
    super(props)
    
    
      this.state = {
        serv:  '',
        cliente:  '',
        equipo:  '',
        ip:  '',
        interfaz: '',
        umbral: 0,
        estado:  'Activo',
        errors: {},
        _id: '',
        id_interface: false ,
        tipo: "Cliente"
     }
    this.onSave = this.onSave.bind(this);
    this.onLoad = this.onLoad.bind(this);
    this.onClose = this.onClose.bind(this);
    this.blankUser = this.blankUser.bind(this);
  }
  
   onLoad() {
     
     if(this.props.create_client_ui.get('edit_modal_client')){
       
       
       
       let client = this.props.create_client_ui.get('edit_modal_client')
       
       this.setState({
       _id: client.get('_id'),
       serv: client.get('serv'),
       cliente: client.get('cliente'),
       equipo: client.get('equipo'),
       estado: client.get('estado'),
       ip: client.get('ip'),
       umbral: client.get('umbral'),
       tipoServicio: client.get('tipoServicio'),
       id_interface: client.get('id_interface'),
       interfaz: client.get('interfaz'),
       tipo: client.get("tipo")

     })
    }
   }
  
   handleChange = (e) => {
        console.log(e.target.name)
        console.log(e.target.value)
        if(!!this.state.errors[e.target.name]){
            let errors = Object.assign({}, this.state.errors)
            delete errors[e.target.name]
            this.setState({
                [e.target.name]: e.target.value,
                errors
            })
        }else{
            this.setState({ [e.target.name]: e.target.value})    
        }
        
    }
  
  
   validateClient(){
       
     let errors = {};
        if(this.props.equipment_interfaces.get("interfaces") && this.state.id_interface){
            if(!this.state.equipo && !this.state.interfaz )
                this.setState({
                    equipo: this.props.equipment_interfaces.get("equipo"),
                    interfaz: this.props.equipment_interfaces.get("interfaces").filter(el=> el.get("id_interface")==this.state.id_interface).toJS()[0].name
                })
        }else{
            console.log(this.state)
            if(/^\s+$/.test(this.state.interfaz))
            errors.interfaz = "Debe seleccionar una interfaz"
        }
        if(this.state.serv === '') errors.serv = "Este campo es obligatorio"
        if(!/^\d+$/.test(this.state.serv)) errors.serv = "Este campo solo acepta valores numericos. Ingrese el numero de SERV"
        if(this.state.cliente === '') errors.cliente = "Este campo es obligatorio"
        if(this.state.umbral === '') errors.umbral = "Este campo es obligatorio"
        if(this.state.id_interface === false) errors.id_interface = 'Seleccione una interfaz'
        if(!/^\d+$/.test(this.state.umbral)) errors.umbral = "Este campo solo acepta valores numericos. Ingrese el numero de SERV"
        if(this.state.ip === '') errors.ip = "Este campo es obligatorio"
        if(!/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(this.state.ip)) errors.ip ="No es una ip válida"
        this.setState({errors})
        const isValid = Object.keys(errors).length === 0
        console.log(errors)
        if(isValid) return true
        return false
   }
  
  
  
   onSave() {
     
    if(this.validateClient()){
        const action = this.props.create_client_ui.get('edit_modal_action')
        
        switch(action){
            case "POST":
                return this.saveNewClient()
                
            case "PUT":
                return this.editClient()
                
            default:
                return null
        }
    }
  }
  
  blankUser(){
   this.setState({
        serv:  '',
        cliente:  '',
        equipo:  '',
        sitio: '',
        ip:  '',
        email: '',
        umbral: '',
        interfaz: '',
        estado:  'Activo',
        errors: {},
        _id: '',
        tipo: "Cliente"
     })
  }
  
  onClose() {
    this.props.hideEditModal()
    this.props.clearInterfaceState()
    this.blankUser()
  }

  
  saveNewClient(){
        
    
            const {tipo,serv, cliente,  ip, estado,  umbral, id_interface} = this.state
            
            
            let equipo= this.props.equipment_interfaces.get("equipo")
            let interfaz= this.props.equipment_interfaces.get("interfaces").filter(el=> el.get("id_interface")==this.state.id_interface).toJS()[0].name
            
            
            
            this.props.setEditModalLoading(true)
            this.props.postNewClient({cliente: {serv,cliente,equipo,id_interface,ip, estado, interfaz, umbral, tipo}}).then(
                (data) => {
                            this.props.setEditModalLoading(false)
                            this.props.hideEditModal()
                            this.props.clearInterfaceState()
                            console.log(data)
                            this.blankUser()
                },
                (err) => err.response.json().then(({errors})=> { 
                    this.props.setEditModalLoading(false)
                    this.props.hideEditModal()
                    this.props.clearInterfaceState()
                    this.blankUser()
                }
                )
            );
        
    }
    
  editClient(){
      
      const {serv, cliente, email, equipo, sitio, ip, estado, _id, interfaz, umbral, tipo} = this.state
            
      this.props.setEditModalLoading(true)
      this.props.putClient({cliente: {serv,cliente, equipo,ip, estado, _id, interfaz, umbral, tipo}}).then(
        (data) => {
                    this.props.setEditModalLoading(false)
                    this.props.hideEditModal()
                    this.blankUser()
                    this.props.clearInterfaceState()
                    
        },
        (err) => { 
                        
                    this.props.setEditModalLoading(false)
                    this.props.hideEditModal()
                    this.props.clearInterfaceState()
                    this.blankUser()
                        }
                    
        
            
      );
      
  }
  
  
  render(){
    let form
    if(this.props.create_client_ui.get('edit_modal_action') === "POST"){
        form = (
            <NewClientForm 
                errors={this.state.errors} 
                client={this.props.create_client_ui.get('edit_modal_client')} 
                onChange={this.handleChange} 
                onLoad={this.onLoad}
                onFetchInterfaces={this.props.fetchInterfaces}
                equipment_interfaces={this.props.equipment_interfaces}
                ip={this.state.ip}
            />)    
    }else{
        form = (
            <EditClientForm 
                errors={this.state.errors} 
                client={this.props.create_client_ui.get('edit_modal_client')} 
                onChange={this.handleChange} 
                onLoad={this.onLoad}
                
            />
            
        )
    }
    
    
     
    return (
    <Modal 
      showModal={this.props.create_client_ui.get('showEditModalflag')}
      title={this.props.create_client_ui.get('edit_modal_title')}
      onClose={this.onClose}
      onConfirm={this.onSave}
      
      confirmButtonMessage="Guardar Cambios"
      confirmButtonClass="is-primary"
      cancelButtonClass="is-dark"
      cancelButtonMessage="Cancelar"
      loading={this.props.create_client_ui.get('edit_modal_loading')}
    >
    {form}
    </Modal>
      )
     
     
      
  }
}

function mapStateToProps(state){
        return {
            create_client_ui: state.create_client_ui,
            equipment_interfaces: state.equipment_interfaces
            
        }
    }

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    showEditModal: showEditModal,
    hideEditModal: hideEditModal,
    postNewClient: postNewClient,
    setEditModalLoading: setEditModalLoading,
    putClient: putClient,
    fetchInterfaces: fetchInterfaces,
    clearInterfaceState: clearInterfaceState
  }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(CreateClientModal);





