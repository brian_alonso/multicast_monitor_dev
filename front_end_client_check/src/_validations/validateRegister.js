// Funcion que valida la entrada del RegisterForm
import validator from 'validator'
import validateManyAttrs from './validateManyAttrs'


function validateRegisterStep1(data){
    let errors = {}
    
    
    //valida que los campos no estén vacíos
    validateManyAttrs(  
                        data, 
                        ["nombre","apellido", "email", "usuario"],
                        errors,
                        "Este campo es obligatorio",
                        validator.isEmpty
                     )
    if(!validator.isEmail(data.email)){
        errors.email = "No es un email válido"
    }
    
    
    return {
        errors,
        isValid: Object.keys(errors).length === 0
    }
}


function validateRegisterStep2(data){
    let errors = {}
    
    
    //valida que los campos no estén vacíos
    validateManyAttrs(  
                        data, 
                        ["inmobiliaria"],
                        errors,
                        "Este campo es obligatorio",
                        validator.isEmpty
                     )
    
    
    return {
        errors,
        isValid: Object.keys(errors).length === 0
    }
}


function validateRegisterStep3(data){
    let errors = {}
    
    
    //valida que los campos no estén vacíos
    validateManyAttrs(  
                        data, 
                        ["password", "confirmPassword"],
                        errors,
                        "Este campo es obligatorio",
                        validator.isEmpty
                     )
    if(!validator.equals(data.password, data.confirmPassword)){
        errors.confirmPassword = "La contraseña no coincide"
    }
    
    if(!validator.isLength(data.password,{min:6, max: undefined})){
        errors.password = "La constraseña debe tener al menos 6 caracteres"
    }
    
    return {
        errors,
        isValid: Object.keys(errors).length === 0
    }
}



export { validateRegisterStep1, validateRegisterStep2, validateRegisterStep3 } 

