// funcion que corre la misma función de validación con el mismo mensaje para varios atributos del mismo objeto
// data: objeto con la información
// attrs: array de atributos que deben ser validados
// errors: objeto donde se guardarán los mensajes de error
// message: mensaje de error
// validateFunc: función que realizará la validación


export default function validateManyAttrs(data ,attrs, errors, message, validateFunc){
    attrs.map( (attr)=> {
        if(validateFunc(data[attr])){
            errors[attr] = message
        }
    })
}
