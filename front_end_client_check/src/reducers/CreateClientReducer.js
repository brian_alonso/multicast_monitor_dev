import  { Map} from 'immutable';

export default function(state= Map({
                        
                        showEditModalflag: false,
                        edit_modal_title: '',
                        edit_modal_client: null,
                        edit_modal_action: "POST",
                        edit_modal_loading: false,
                        
  
}), action) {
    
    
  
  
  switch(action.type){
    case "SHOW_EDIT_MODAL":
      
       return state.set('showEditModalflag',  true );
    case "HIDE_EDIT_MODAL":
      
       return  state.set('showEditModalflag',  false );  
    case "SET_EDIT_MODAL_TITLE":
      
        return state.set('edit_modal_title', action.payload);
    case "SET_EDIT_MODAL_CLIENT":
      
        return state.set('edit_modal_client', action.payload);
    case "SET_EDIT_MODAL_ACTION":
      
        return state.set('edit_modal_action', action.payload);    
    case "SET_EDIT_MODAL_LOADING":
      
        return state.set('edit_modal_loading', action.payload);
    case "SET_EDIT_CLIENT":
        return state.set('edit_modal_client', action.payload);
    default: return state;
  }  
      
   
}
