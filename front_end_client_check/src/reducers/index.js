import { combineReducers } from 'redux';
import ClientsReducer from './ClientsReducer.js';
import CreateClientReducer from './CreateClientReducer.js'
import DeleteClientReducer from './DeleteClientReducer.js'
import InterfaceStateReducer from './InterfaceStateReducer.js';
import HistoricoReducer from './HistoricoReducer.js'
import TableReducer from './TableReducer'
import SlidePanelReducer from './SlidePanelReducer'
import EquipmentInterfacesReducer from './EquipmentInterfacesReducer'


const allReducers = combineReducers({
    clients: ClientsReducer,
    create_client_ui: CreateClientReducer,
    delete_client_ui: DeleteClientReducer,
    interface_state_ui: InterfaceStateReducer,
    historico: HistoricoReducer,
    table: TableReducer,
    slidePanel: SlidePanelReducer,
    equipment_interfaces: EquipmentInterfacesReducer
});

export default allReducers

