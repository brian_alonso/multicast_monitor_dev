import  { Map, List } from 'immutable';

export default function(state= Map({
                        
                        order_by: 'ultimo_cambio',
                        direction: 'desc',
                        selected: List()
                        
  
}), action) {
    
    
  
  
  switch(action.type){
    
    case "SET_ORDER_BY":
      
        return state.set('order_by', action.payload);
    case "SET_DIRECTION":
      
        return state.set('direction', action.payload);
    case "SELECT_CLIENT":
      return state.set('selected', state.get('selected').push(action.payload) );
        
    case "UNSELECT_CLIENT":
      
        return state.set('selected', state.get('selected').filterNot(
          (el) => el === action.payload
          ) );
    
    default: return state;
  }  
      
   
}
