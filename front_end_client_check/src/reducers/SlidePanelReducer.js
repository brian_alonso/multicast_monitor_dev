import  { Map} from 'immutable';

export default function(state= Map({
                        
                        show: false,
                        loading: false,
                        
  
}), action) {
    
    
  
  
  switch(action.type){
    case "SHOW_SLIDE_PANEL":
      
       return state.set('show',  true );
    case "HIDE_SLIDE_PANEL":
      
      
        return state.set('show', false);
    default: return state;
  }  
      
   
}
