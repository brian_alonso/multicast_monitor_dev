export function showEditModal(){
    
    return {
        type: "SHOW_EDIT_MODAL"
        
    }
}

export function setEditClient(client){
    return {
        type: "SET_EDIT_CLIENT",
        payload: client
    }
}



export  function hideEditModal(){
    
    return {
        type: "HIDE_EDIT_MODAL"
    }
    
}


export function setEditModalTitle(title){
    return {
        type: "SET_EDIT_MODAL_TITLE",
        payload: title
    }
}

export function setEditModalClient(client){
    return {
        type: "SET_EDIT_MODAL_CLIENT",
        payload: client
    }
}

export function setEditModalAction(action){
    return {
        type: "SET_EDIT_MODAL_ACTION",
        payload: action
    }
}

export function setEditModalLoading(loading){
    return {
        type: "SET_EDIT_MODAL_LOADING",
        payload: loading
    }
}

















