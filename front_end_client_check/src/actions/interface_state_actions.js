export function setInterfaceState(state){
    return {
        type: "SET_INTERFACE_STATE",
        payload: state
    }
}

export function clearInterfaceState(state){
    return {
        type: "CLEAR_INTERFACE_STATE",
        payload: state
    }
}

