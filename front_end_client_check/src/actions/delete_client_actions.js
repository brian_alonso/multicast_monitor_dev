export function showDeleteClientModal(){
    
    return {
        type: "SHOW_DELETE_CLIENT_MODAL"
        
    }
}


export  function hideDeleteClientModal(){
    
    return {
        type: "HIDE_DELETE_CLIENT_MODAL"
    }
    
}


export function setDeleteClientModalLoading(loading){
    return {
        type: "SET_DELETE_CLIENT_MODAL_LOADING",
        payload: loading
    }
}

export function setDeleteClientId(id){
    return {
        type: "SET_DELETE_CLIENT_ID",
        payload: id
    }
}





