import io from 'socket.io-client';
import {updateClient} from "../actions/client_rest_actions"
import {setInterfaceState} from "../actions/interface_state_actions"
import store from "../index"

var lastUpdate = false

var socket = io.connect('/');
  socket.on('update_situacion', function (data) {
    //console.log(data);
    store.dispatch(updateClient(data));    
    lastUpdate = new Date();
  });
  socket.on('heart_beat', function (data) {
   // console.log(data);
    lastUpdate = new Date();
    //store.dispatch(setLastUpdate());    
  });

function compareDates() {
  if(!lastUpdate) return false
  //console.log("Comparing dates");
  let now = new Date()
  let diff =  now - lastUpdate
  //console.log(diff);
  if (diff > 80000)
    store.dispatch(setInterfaceState("Down"))
  else
    store.dispatch(setInterfaceState("Up"))
} 
var id = setInterval(compareDates, 10000)


  
export default socket
