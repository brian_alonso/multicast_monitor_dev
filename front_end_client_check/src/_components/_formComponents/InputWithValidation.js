/*
    Este componente integra el input con la validación
    Ejemplo de uso:
    <InputWithValidation
        errors = { errors.inmobiliaria }
        Change = { this._onChange }
        placeholder = "Nombre de la inmobiliaria"
        value = { inmobiliaria }
        name = "inmobiliaria"
    />

*/

import React, { Component} from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'



class InputWithValidation extends Component {
    
  
    
    render(){
    
        const { label, name, errors, type, placeholder, value, _onChange, disabled } = this.props
        let disable = {}
        if(disabled){
            disable["disabled"]="disabled"
        }
        return(
            
            <div className="field">
                <label className="label is-small">{ label }</label>
                <div className="control">
                    <input
                        onChange={ _onChange}
                        className={classnames('input is-small',{'is-danger': !!errors})}
                        type={ type } 
                        placeholder={ placeholder }
                        value={ value }
                        name={ name }
                        {...disable}
                    />
                    <p className="help is-danger">
                        { errors }
                    </p>
                </div>
                
            </div>
            
            
            
            
            
        )
    }
}

 InputWithValidation.defaultProps ={
     type: "text",
     placeholder: ""
 }
 
 InputWithValidation.propTypes = {
     name: PropTypes.string.isRequired,
     errors: PropTypes.string,
     placeholder: PropTypes.string.isRequired,
     type: PropTypes.string,
     value: PropTypes.string,
     _onChange: PropTypes.func.isRequired
    
 }






export { InputWithValidation } 

