import React, { Component } from 'react';
import classnames from 'classnames';

//Description: Modal dialog confirm. Only two buttons Accept and Cancel Customizable.
//Use:
// <Modal 
//       showModal=  boolean to show o hide modal
//       title= string with the title
//       onClose= function called when press button cancel
//       onConfirm= function called when press  confirm button
//       confirmButtonMessage= string with the button confirm message
//       confirmButtonClass= string with css class 
//       cancelButtonClass= string with css class
//       cancelButtonMessage= string with the cancel button message
//       loading= boolean to show confirm booton with class loading
//       content= jsx with content it is a replace for children ie: ={  <Contacts />}
// >
//     children 
//     </Modal>




class Modal extends Component {
  render(){
    
    if(!this.props.showModal) return null
    else {
    
    
    return (
    
    <div className={classnames('modal', {'is-active':this.props.showModal})}>
  <div className="modal-background"></div>
  <div className="modal-card">
    <header className="modal-card-head">
      <p className="modal-card-title">{this.props.title}</p>
      <button className="delete" onClick={this.props.onClose} aria-label="close"></button>
    </header>
    <section className="modal-card-body">
      {this.props.children}
      {this.props.content}
    </section>
    <footer className="modal-card-foot">
      <button className={classnames('button', this.props.confirmButtonClass ,{'is-loading': this.props.loading})} onClick={this.props.onConfirm}>{this.props.confirmButtonMessage}</button>
      <button className={classnames('button', this.props.cancelButtonClass )} onClick={this.props.onClose}>{this.props.cancelButtonMessage}</button>
    </footer>
  </div>
</div>)
    }
     
      
  }
}

export default Modal