import React, { Component } from 'react'
import { Line } from 'react-chartjs-2' 
import moment from 'moment'


class StateGraph extends Component{
/*
  StateGraph resuelve el historico en formato crudo, lo adapta y genera la data.
  Además recibe el min y max para los límites de la gráfrica en los props

 <StateGraph historico={} > 

*/

  //console.log(historico)

  adaptar(historico, situacion){
    console.log(historico)
    if (!historico) return ;
    let adaptado = historico.map( (el) => {
        let y = el.trafico
        return { t: moment(el.ultimo_cambio), y: y }
    }).concat({
      t: moment(), y: historico[historico.length -1].trafico
    })
    console.log(adaptado)
    return adaptado
    
  }
 

  data(){
    return {
     datasets: [
        {       
          label: "Tráfico",
          steppedLine: true,          
          data: this.adaptar(this.props.historicos), 
          fill: true,
          backgroundColor: "#c6ffb3",
        }
       
      
      ]
      }
    }
  
 
 
  render(){

    var chartData = this.data()
    var options = {
      maintainAspectRatio: true,
      responsive: true,
      scales: {
        yAxes: [
          {
            
            ticks: {
              maxTicksLimit: 1,
              beginAtZero: true
            }

          }
        ],
        xAxes: [
          {
            type: 'time',
            distribution: 'linear',
            time:{
              max: this.props.max,
              min: this.props.min,
              displayFormats:{
              }
            }
          }
        ]
        
      },
      tooltips:{
        callbacks:{
          title: function(tooltipItem, chart){
            console.log(tooltipItem)
            return moment(tooltipItem[0].xLabel).calendar()
          }
        }
      }
            }
    
    return(
      <div>
        <div className="content">
          <h4>Linea de tiempo</h4>
          {this.props.children}
        </div>
        < Line data={chartData} options={options} width={900} height={100}/> 
      </div>
    );
  }
}


export default StateGraph
