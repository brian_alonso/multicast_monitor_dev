import React, { Component} from 'react'
import classnames from 'classnames'
import moment from 'moment'
class HistoricoTable extends Component{
  
  //debe recibir un array de Titulos para las pestañas
  //y debe recibir un handler para cuando le doy click
  //y debe recibir que label está activo
  
  render(){

    var header = function(titles){
      let h = titles.map( (title)=>{
        return(
          <th key={title}>{title}</th>
        )
      })
        return <thead><tr>{h}</tr></thead>
    }

    var frase = function(situacion){
      let frases = {
        "Up": "arriba",
        "Down": "caido",
        "Desactivado":"sin monitoreo"
      }
      return frases[situacion]
    }  
    
    var style = function(situacion){
      let styles= {
        "Up": "green-up",
        "Down": "red-down",
        "Desactivado":"blue-pause"
      }
      return styles[situacion]
    }

   
    var content = function(historico){
      let cambio_anterior = new Date()
      let c = historico.map( (el, i, array)=>{
        
         if (array[i-1]){
          cambio_anterior = array[i-1].ultimo_cambio 
         }else{
          cambio_anterior = new Date()
         }
        let diff = moment(cambio_anterior) - moment(el.ultimo_cambio)
        diff = moment.duration(diff, 'milliseconds')
        let hours = diff.hours()
        let mins = diff.minutes()
        let seconds = diff.seconds()
        let days = diff.days()
        return(
        <tr className={style(el.situacion) } key={el._id}>
          <td>{el.situacion}</td>
          <td > {diff.humanize()} {frase(el.situacion)}  <span className="achicar">({days>0 ? days + " días":" "} {hours}:{mins}:{seconds} )</span> </td>
          <td>{moment(el.ultimo_cambio).format('lll')}</td>
        </tr>
        )
      })
      return <tbody>{c}</tbody>
    }
    
    return( 
      <div >
        
        <div className="content">
          <h4>Listado de cambios de estado</h4>
        </div>
      <table className="table is-bordered is-narrow ">
          {header(this.props.titles)}
          {content(this.props.historicos)}
        </table>
      </div>
    );
  }
}

export default HistoricoTable
