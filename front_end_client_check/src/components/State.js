import React, { Component } from 'react';
import classnames from 'classnames';
import moment from 'moment'
import { Link } from 'react-router-dom';
import ReactGauge from 'react-gauge-capacity';
import Gauge from './Gauge'
let esLocale = require('moment/locale/es')
moment.locale('es', esLocale)



class State extends Component {
    

    
          
  
  
  
  
  render(){
      
      
   
      
    var articleClass = "is-success"
    var icon = "fa-check"
    
    switch(this.props.estado){
        case "Up":
            if(moment.duration(moment().diff(moment(this.props.date))).asHours()>4){
                articleClass = "is-success"
                icon = "fa-check"
                
            }else{
                articleClass = "is-success"
                icon = "fa-exclamation"
            }
            break
        case "Desactivado":
            articleClass = "is-dark"
            icon = "fa-pause"
            break
        default: 
            articleClass = "is-danger"
            icon = "fa-bell"
            
        
    }
    
    
    return (
        <div className="column is-2-fullhd is-3-widescreen is-4-desktop is-5-tablet is-11-mobile">
            <article className={classnames('message is-small',articleClass)}>
              <div className="message-header">
                <p>
                    <Link to={"/clients/" + this.props._id} >
                        SERV-{this.props.serv}
                    </Link>
                </p>
                    <span className="icon is-small">
                        <i className={classnames('fa', icon)}></i>
                    </span>
         
              </div>
              <div className="message-body">
              <div className="columns is-gapless">
                
                <div className="column">
                    <ul>
                        <li><strong>{this.props.nombreCliente}</strong></li>
                        <li>{this.props.ag}</li>
                        <li>{this.props.sitio}</li>
                        <li>{this.props.ip}</li>
                        <li>{this.props.interfaz}</li>
                        <li>Umbral: {this.props.umbral}</li>
                        <li>Diagnóstico: {this.props.estado}</li>
                        <li>{moment(this.props.date).calendar()}</li>
                        
                    </ul>
                </div>
                <div className="column is-half">
                    <Gauge level={this.props.trafico} threshold={this.props.umbral}/>
                </div>
              </div>
                
             </div> 
            </article>
        </div>
     
      );
  }
}

export default State
