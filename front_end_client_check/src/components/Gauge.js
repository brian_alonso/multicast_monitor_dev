import React, { Component } from 'react';
import classnames from 'classnames';
import moment from 'moment'
import { Link } from 'react-router-dom';
import ReactGauge from 'react-gauge-capacity';
let esLocale = require('moment/locale/es')
moment.locale('es', esLocale)



class Gauge extends Component {
    
  constructor(props){
   super(props)
   this.to180base = this.to180base.bind(this);
 }
    
          
  to180base(x){
      return (180 * x) /100
  }
  
  
  
  render(){
      
    
      
    const { level, threshold  } = this.props
    
    // let level180 = 180 * level / 100
    // let threshold = 180 * threshold / 100
    
      
    let options = {
    	isInnerNumbers: false, 
    	aperture: 180, 
    	radius : 55,
    	tickOffset: 2,
    	arcStrokeWidth: 25,
    	miniTickLength: 1,
    	miniTickStrokeWidth: 1,
    	tickLabelOffset: 22,
    	scaleDivisionNumber: 2,
    	centralCircleRadius: 10,
    	marks: ["0%","%25","%50","%75","%100"],
    	contentWidth: 180,
    	svgContainerWidth: 120,
    	svgContainerHeight: 90,
    	
    	arrowValue: this.to180base(level)/180,
    	gaugeCenterLineHeight: 80,
    	viewBox: "77 -12 65 115",
    	
    	ranges: [{
    			start: 0,
    			end: this.to180base(threshold)/180,
    			color: "#ff8080"
    		},
    	
    		{
    			start: this.to180base(threshold)/180,
    			end: 180/180,
    			color: "#99e699"
    		},
    		
    		
    		]
    
    }
          
      
      
    
    return (
                    <span>
                    <ReactGauge {...options} />
                    <div className="has-text-centered" style={{paddingRight: -10}}>%{level}</div>
                    </span>
      );
  }
}

export default Gauge
