import React, { Component } from 'react';
import classnames from 'classnames';


class NewClientForm extends Component {
    
 
  componentDidMount(){
    this.props.onLoad()
  }
  
  render(){
   
   
   
    let renderInterfaces= (interfaces)=>{
      
      if(interfaces.get("error")){
        return(
          <div className="has-text-danger">
                Error: {interfaces.get("error")}
          </div>
        )
      }
      if(interfaces.get("interfaces")){
      return(
      <div className="field">
        <label className="label">Interfaces</label>
        <div className="control">
          <div className={classnames('select',{'is-danger': !!this.props.errors.id_interface})} >
      
       <select 
              name="id_interface" 
              onChange={this.props.onChange}
              
              >
              <option value={false}>
               Seleccione interfaz
              </option>
              { 
                interfaces.get('interfaces').map((interfaz, index) => (
                <option value = { interfaz.get('id_interface')} >
                  
                  { interfaz.get('name')  } ({ interfaz.get('state')} Tráfico in: { interfaz.get('traffic_in')}%, out: { interfaz.get('traffic_out')}%)
                  
                </option>)
              )
                
              }
       </select>
        <p className="help is-danger">
            {this.props.errors.id_interface}
          </p>
     
       </div>
       </div>
       <br />
       <label className="label">Equipo</label>
        <div className="control">
          { interfaces.get("equipo")}
        </div>
       
       </div>
      )
      }
      return
    
  }
   
    
    return (
    <div>
      <div className="field">
        <label className="label">Dirección IP Equipo</label>      
        <div className="field is-grouped">
        
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.ip})} 
                    name="ip"  
                    onChange={this.props.onChange} 
                    defaultValue={this.props.client && this.props.client.get('ip')}
                    type="text" 
                    placeholder="1.2.3.4" />
          </div>
          <button 
            disabled={!/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(this.props.ip)}
            className={classnames('button is-info is-outlined', {'is-loading': false})} 
            onClick={()=>this.props.onFetchInterfaces(this.props.ip)}
          >
            Descubrir interfaces
          </button>
         
          
      </div>
      <p className="help is-danger">
            {this.props.errors.ip}
      </p>
      </div>
           { renderInterfaces(this.props.equipment_interfaces)}
      <div className="field">
        <label className="label">Umbral</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.umbral})} 
                    name="umbral"
                    defaultValue={this.props.umbral && this.props.client.get('umbral')}
                    onChange={this.props.onChange} 
                    type="number" 
                    placeholder="Por debajo de este valor se generará alarmas" />
          </div>
          <p className="help is-danger">
            {this.props.errors.umbral}
          </p>
      </div>
        <div className="field">
        <label className="label">Tipo de servicio</label>
        <div className="control">
          <div className="select">
      
       <select 
              name="tipo" 
              onChange={this.props.onChange}
        >
                <option value = "Cliente" >Cliente</option>
                <option value = "Fuente"  >Fuente</option>
       </select>
       </div>
       </div>
       </div>
      <div className="field ">
        <label className="label is-danger">Servicio</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.serv})} 
                    name="serv"
                    defaultValue={this.props.client && this.props.client.get('serv')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Numero de SERV" />
          </div>
          <p className="help is-danger">
            {this.props.errors.serv}
          </p>
      </div>
      <div className="field">
        <label className="label">Cliente</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.cliente})} 
                    name="cliente"
                    defaultValue={this.props.client && this.props.client.get('cliente')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Nombre del cliente" />
          </div>
          <p className="help is-danger">
            {this.props.errors.cliente}
          </p>
      </div>

              
    </div>
    
    )
     
     
      
  }
}

export default NewClientForm



/*


   <div className="field">
        <label className="label">Sitio</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.sitio})} 
                    name="sitio"  
                    defaultValue={this.props.client && this.props.client.get('sitio')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Nombre del sitio" />
          </div>
          <p className="help is-danger">
            {this.props.errors.sitio}
          </p>
      </div>
      <div className="field">
        <label className="label">Equipo</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.equipo})} 
                    name="equipo"  
                    defaultValue={this.props.client && this.props.client.get('equipo')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Equipo IP al cual se conecta" />
          </div>
          <p className="help is-danger">
            {this.props.errors.equipo}
          </p>
      </div>
   


*/