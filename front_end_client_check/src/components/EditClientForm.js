import React, { Component } from 'react';
import classnames from 'classnames';


class EditClientForm extends Component {
    
 
  componentDidMount(){
    this.props.onLoad()
  }
  
  render(){
   console.log(this.props.client.toJS())
   return (
    <div>
      <div className="field">
        <label className="label">Umbral</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.umbral})} 
                    name="umbral"
                    defaultValue={this.props.client && this.props.client.get('umbral')}
                    onChange={this.props.onChange} 
                    type="number" 
                    placeholder="Por debajo de este valor se generará alarmas" />
          </div>
          <p className="help is-danger">
            {this.props.errors.umbral}
          </p>
      </div>
        <div className="field">
        <label className="label">Tipo de servicio</label>
        <div className="control">
          <div className="select">
      
       <select 
              name="tipo" 
              onChange={this.props.onChange}
              defaultValue={this.props.client && this.props.client.get('tipo')}
        >
                <option value = "Cliente" >Cliente</option>
                <option value = "Fuente"  >Fuente</option>
       </select>
       </div>
       </div>
       </div>
      <div className="field ">
        <label className="label is-danger">Servicio</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.serv})} 
                    name="serv"
                    defaultValue={this.props.client && this.props.client.get('serv')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Numero de SERV" />
          </div>
          <p className="help is-danger">
            {this.props.errors.serv}
          </p>
      </div>
      <div className="field">
        <label className="label">Cliente</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.cliente})} 
                    name="cliente"
                    defaultValue={this.props.client && this.props.client.get('cliente')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Nombre del cliente" />
          </div>
          <p className="help is-danger">
            {this.props.errors.cliente}
          </p>
      </div>

              
    </div>
    
    )
     
     
      
  }
}

export default EditClientForm



/*


   <div className="field">
        <label className="label">Sitio</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.sitio})} 
                    name="sitio"  
                    defaultValue={this.props.client && this.props.client.get('sitio')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Nombre del sitio" />
          </div>
          <p className="help is-danger">
            {this.props.errors.sitio}
          </p>
      </div>
      <div className="field">
        <label className="label">Equipo</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.equipo})} 
                    name="equipo"  
                    defaultValue={this.props.client && this.props.client.get('equipo')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Equipo IP al cual se conecta" />
          </div>
          <p className="help is-danger">
            {this.props.errors.equipo}
          </p>
      </div>
   


*/