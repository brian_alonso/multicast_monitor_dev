import moment from 'moment'
import _ from 'underscore'


export default function(hist,min,max){
    
     let total = {
      Up: 0,
      Down: 0,
      Desactivado: 0,
    }
    
    
    
    let h = _.map(hist, _.clone)
    h.push({situacion: "fin", ultimo_cambio: moment().subtract(0.5,'seconds')})
    let size = h.length - 1
    if(size === 1){
        total[h[0].situacion] += max - moment(h[0].ultimo_cambio)
        return total
    }
    let primero = null
    let ultimo = null
    
    
    for(var i = 0; i < size; i++){
        if(moment(h[i].ultimo_cambio) >= min && moment(h[i].ultimo_cambio) <= max && moment(h[i+1].ultimo_cambio) <= max  ){
            total[h[i].situacion] +=  moment(h[i+1].ultimo_cambio) - moment(h[i].ultimo_cambio)
            if(!primero){
                primero = true
                if (h[i-1]){
                total[h[i-1].situacion] += moment(h[i].ultimo_cambio) - min
                }
            }
            ultimo = i
            
        }
        else if(moment(h[i].ultimo_cambio) >= min && moment(h[i].ultimo_cambio) <= max && moment(h[i+1].ultimo_cambio) >= max){
            ultimo = i - 1
            
        }
      
    }
    
    if(ultimo || ultimo==0){
        total[h[ultimo+1].situacion] += max - moment(h[ultimo+1].ultimo_cambio)
        if( !primero){
            total[h[ultimo].situacion] += min - moment(h[ultimo].ultimo_cambio)
        }
    }else{
        let situacion = size -1
         for(var i = 0; i < size; i++){
            if(moment(h[i].ultimo_cambio) > min){
                situacion = i -1 
                size = i
            }
         }
        total[h[situacion].situacion] += max - min
        
    }
    
   
   return total
    
    
}


